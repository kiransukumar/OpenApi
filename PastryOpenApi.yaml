---
openapi: 3.1.0
info:
  title: PastryOpenApi
  version: 2.0.0
  description: API definition of API Pastry sample app
  contact:
    name: Laurent Broudoux
    url: http://github.com/lbroudoux
    email: laurent.broudoux@gmail.com
  license:
    name: MIT License
    url: https://opensource.org/licenses/MIT
paths:
  /pastry:
    summary: Global operations on pastries
    get:
      tags:
        - pastry
      responses:
        "200":
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/Pastries'
              examples:
                pastries:
                  value: |-
                    [
                    {
                      "name": "{{ randomFullName() }}",
                      "description": "{{ randomString(64) }}",
                      "size": "{{ randomValue(S,M,L) }}",
                      "price": 2.5,
                      "status": "{{ randomValue(available, out_of_stock) }}"
                    },
                    {
                      "name": "{{ randomFullName() }}",
                      "description": "{{ randomString(64) }}",
                      "size": "{{ randomValue(S,M,L) }}",
                      "price": 2.5,
                      "status": "{{ randomValue(available, out_of_stock) }}"
                    },
                    {
                      "name": "{{ randomFullName() }}",
                      "description": "{{ randomString(64) }}",
                      "price": 6.5,
                      "status": "{{ randomValue(available, out_of_stock) }}"
                    }
                    ]
          description: Get list of pastries
      operationId: GetPastries
      summary: Get list of pastries
  /pastry/{name}:
    summary: Specific operation on pastry
    get:
      parameters:
        - examples:
            Eclair Cafe:
              value: Eclair Cafe
            Eclair Cafe Xml:
              value: Eclair Cafe
            Millefeuille:
              value: Millefeuille
          name: name
          description: pastry name
          schema:
            type: string
          in: path
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Pastry'
              examples:
                Eclair Cafe:
                  value:
                    name: Eclair Cafe
                    description: Delicieux Eclair au Cafe pas calorique du tout
                    size: M
                    price: 2.5
                    status: available
                Millefeuille:
                  value: |-
                    {
                      "name": "{{ randomFullName() }}",
                      "description": "{{ randomString(64) }}",
                      "size": "{{ randomValue(S,M,L) }}",
                      "price": 2.5,
                      "status": "{{ randomValue(available, out_of_stock) }}"
                    }
            text/xml:
              schema:
                $ref: './PastrySchema.json'
              examples:
                Eclair Cafe Xml:
                  value: |-
                    <pastry>
                        <name>Eclair Cafe</name>
                        <description>Delicieux Eclair au Cafe pas calorique du tout</description>
                        <size>M</size>
                        <price>2.5</price>
                        <status>available</status>
                    </pastry>
          description: Pastry with specified name
      operationId: GetPastryByName
      summary: Get Pastry by name
      description: Get Pastry by name
    patch:
      requestBody:
        content:
          application/json:
            schema:
              $ref: './PastrySchema.json'
            examples:
              Eclair Cafe:
                value:
                  price: 2.6
          text/xml:
            schema:
              $ref: './PastrySchema.json'
            examples:
              Eclair Cafe Xml:
                value: "<pastry>\n\t<price>2.6</price>\n</pastry>"
        required: true
      parameters:
        - examples:
            Eclair Cafe:
              value: Eclair Cafe
            Eclair Cafe Xml:
              value: Eclair Cafe
          name: name
          description: pastry name
          schema:
            type: string
          in: path
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: './PastrySchema.json'
              examples:
                Eclair Cafe:
                  value:
                    name: Eclair Cafe
                    description: Delicieux Eclair au Cafe pas calorique du tout
                    size: M
                    price: 2.6
                    status: available
            text/xml:
              schema:
                $ref: './PastrySchema.json'
              examples:
                Eclair Cafe Xml:
                  value: |-
                    <pastry>
                        <name>Eclair Cafe</name>
                        <description>Delicieux Eclair au Cafe pas calorique du tout</description>
                        <size>M</size>
                        <price>2.6</price>
                        <status>available</status>
                    </pastry>
          description: Changed pastry
      operationId: PatchPastry
      summary: Patch existing pastry
    parameters:
      - name: name
        description: pastry name
        schema:
          type: string
        in: path
        required: true
components:
  schemas:
    Pastries:
      type: array
      items:
        $ref: '#/components/schemas/Pastry'
    Pastry:
      type: object
      $ref: './PastrySchema.json'
tags:
  - name: pastry
    description: Pastry resource
